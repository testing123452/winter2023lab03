import java.util.Scanner;

public class ApplianceStore
{
	public static void main(String[] args)
	{
		Refrigerator[] fridge = new Refrigerator[4];
		Scanner scan = new Scanner(System.in);

		for (int i = 0; i < fridge.length; i++)
		{
			fridge[i] = new Refrigerator();
			System.out.println("How many shelves");
			fridge[i].shelves = scan.nextInt();

			System.out.println("How many drawers");
			fridge[i].drawers = scan.nextInt();

			System.out.println("What height");
			fridge[i].height = scan.nextDouble();
		}

		System.out.println("Shelves: " + fridge[3].shelves);
		System.out.println("Drawers: " + fridge[3].drawers);
		System.out.println("Height: " + fridge[3].height);

		fridge[0].howManyShelves();
		fridge[0].howManyDrawers();
	}
}