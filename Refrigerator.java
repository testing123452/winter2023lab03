public class Refrigerator
{
	int shelves;
	int drawers;
	double height;
	
	public void howManyShelves()
	{
		System.out.println("Number of shelves: " + shelves);
	}
	
	public void howManyDrawers()
	{
		System.out.println("Number of drawers: " + drawers);
	}
}